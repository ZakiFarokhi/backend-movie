class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable
         def self.register(email, password)
          user = create({
            :email => email,
            :password => password
          })
          user.save! ? user : nil
        end
      
        def self.authenticate(email, password)
          user = find_for_authentication(email: email)
          return [false, "User doesn't exist!"] if user.blank?
          return [false, "Wrong password"] unless user.try(:valid_password?, password)
      
          token = generate_token(user)
      
          return [
            true, 
            {
              id: user.id,
              email: user.email,
              token: token.token
            }
          ]
        end
      
        private
      
        def self.generate_token(user)
          Doorkeeper::AccessToken.create!(
            application_id: nil,
            resource_owner_id: user.id,
            expires_in: 24.hours,
            scopes: "public"
          )
        end
      
end
