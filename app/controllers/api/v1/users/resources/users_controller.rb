module Api::V1::Users::Resources
    class UsersController < Grape::API
        resource "auth" do
            params do
              requires :email, type: String
              requires :password, type: String
            end
            post "/register" do
              user = User.register(params[:email], params[:password])
              return errors!({
                success: false,
                errors: "Failed to register"
              }, 422) if user.blank?
      
              data = {
                success: true,
                data: user
              }
      
              present data
            end
      
            params do
              requires :email, type: String
              requires :password, type: String
            end
            post '/login' do
              user = User.authenticate(params[:email], params[:password])
              return error!({
                success: user.first,
                errors: user.second
              }, 401) unless user.first
      
              data = {
                success: true,
                data: user.second
              }
      
              present data
            end
      
            oauth2
            get '/protected' do
              data = {
                success: true,
                data: "I'm protected!"
              }
      
              present data
            end
          end
    end
end

