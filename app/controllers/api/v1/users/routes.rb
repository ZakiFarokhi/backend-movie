module Api::V1::Users
    class Routes < Grape::API
        # formatter :json, SuccessFormatter
        # error_formatter :json, ErrorFormatter

        mount Api::V1::Users::Resources::UsersController
    end
end