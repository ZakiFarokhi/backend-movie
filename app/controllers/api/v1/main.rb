module Api
    module V1
        class Main < Grape::API
            prefix "api"
            format :json
            version "v1", using: :path
            use ::WineBouncer::OAuth2

            mount Api::V1::Users::Routes
            mount Api::V1::Movies::Routes
            mount Api::V1::Castings::Routes
             # Swagger config
        add_swagger_documentation(
            api_version:             "v1",
            doc_version:             "v1",
            hide_documentation_path: true,
            mount_path:              "documentation.json",
            hide_format:             true,
            info: {
                title: "Web API Collection",
                description: "A collection for Web client."
            }
          )
        
        end
    end
end
