# frozen_string_literal: true
module Api::V1::Castings::Resources
  class Castings < Grape::API
    resource 'castings' do
      desc 'get all Castings'
      get '/', http_codes: [
        [200, 'success'],
        [401, 'Invalid Access Token']
      ] do
        results = Casting.select.all.order('updated_at DESC')
        present result: results
      end
  
      desc 'get Casting by id'
      params do
        requires :id, type: String, desc: 'Casting ID'
      end
      get ':id', http_codes: [
        [200, 'success'],
        [401, 'Unauthorized']
      ] do
        result = Casting.select.find(permitted_params[:id])
        present result: result
      end
  
      desc 'Add Casting'
      params do
        requires :name,
                 type: String,
                 desc: 'Casting name',
                 documentation: { param_type: 'body' }
        requires :age,
                 type: Integer,
                 desc: 'Casting age',
                 documentation: { param_type: 'body' }
        optional :image,
                 type: String,
                 desc: 'Casting image',
                 documentation: { param_type: 'body' }
      end
      oauth2
      post '/', http_codes: [
        [201, 'created successfully'],
        [401, 'Invalid Access Token']
      ] do
        results = Casting.create!(permitted_params)
        present result: results
      end
  
      desc 'Update Casting'
      params do
        requires :id, type: String, desc: 'Casting ID'
        requires :name,
                 type: String,
                 desc: 'Casting Name',
                 documentation: { param_type: 'body' }
        requires :age,
                 type: Integer,
                 desc: 'Casting age',
                 documentation: { param_type: 'body' }
        optional :image,
                 type: String,
                 desc: 'Casting image',
                 documentation: { param_type: 'body' }
      end
      oauth2 
      put ':id',  http_codes: [
        [200, 'success'],
        [401, 'Invalid Access Token']
      ] do
        results = Casting.find(permitted_params[:id])
        results.update!(permitted_params)
        present result: results
      end
    end
  end
  
end
