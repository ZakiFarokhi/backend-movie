module Api::V1::Castings
  class Routes < Grape::API
      # formatter :json, SuccessFormatter
      # error_formatter :json, ErrorFormatter

      mount Api::V1::Castings::Resources::Castings
  end
end