# frozen_string_literal: true
module Api::V1::Movies::Resources
  class Movies < Grape::API
    resource 'movies' do
      desc 'get all movies'
      get '/', http_codes: [
        [200, 'success'],
        [401, 'Invalid Access Token']
      ] do
        results = Movie.select.all.order('updated_at DESC')
        present result: results
      end
  
      desc 'get movie by id'
      params do
        requires :id, type: String, desc: 'Movie ID'
      end
      get ':id', http_codes: [
        [200, 'success'],
        [401, 'Unauthorized']
      ] do
        result = Movie.select.find(permitted_params[:id])
        present result: result
      end
  
      desc 'Add movie'
      params do
        requires :title,
                 type: String,
                 desc: 'movie title',
                 documentation: { param_type: 'body' }
        requires :desc,
                 type: String,
                 desc: 'Movie label',
                 documentation: { param_type: 'body' }
        optional :image,
                 type: String,
                 desc: 'Movie image',
                 documentation: { param_type: 'body' }
      end
      oauth2
      post '/', http_codes: [
        [201, 'created successfully'],
        [401, 'Invalid Access Token']
      ] do
        results = Movie.create!(permitted_params)
        present result: results
      end
  
      desc 'Update movie'
      params do
        requires :id, type: String, desc: 'Movie ID'
        requires :title,
                 type: String,
                 desc: 'Movie title',
                 documentation: { param_type: 'body' }
        requires :desc,
                 type: String,
                 desc: 'Movie desc',
                 documentation: { param_type: 'body' }
        optional :image,
                 type: String,
                 desc: 'Movie image',
                 documentation: { param_type: 'body' }
      end
      oauth2 
      put ':id',  http_codes: [
        [200, 'success'],
        [401, 'Invalid Access Token']
      ] do
        results = Movie.find(permitted_params[:id])
        results.update!(permitted_params)
        present result: results
      end
    end
  end  
end
