module Api::V1::Movies
  class Routes < Grape::API
      # formatter :json, SuccessFormatter
      # error_formatter :json, ErrorFormatter

      mount Api::V1::Movies::Resources::Movies
  end
end