class CreateMovies < ActiveRecord::Migration[6.0]
  def change
    create_table :movies do |t|
      t.string :title
      t.string :desc
      t.string :image
      t.integer :view

      t.timestamps
    end
  end
end
