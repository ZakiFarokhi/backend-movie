class CreateCastings < ActiveRecord::Migration[6.0]
  def change
    create_table :castings do |t|
      t.string :name
      t.integer :age
      t.string :image
      t.references :movie, null: false, foreign_key: true

      t.timestamps
    end
  end
end
