class AddCastingRefToMovies < ActiveRecord::Migration[6.0]
  def change
    add_reference :movies, :casting, null: false, foreign_key: true
  end
end
